# IoT-TEG

IoT–TEG (Internet of Things–Test Event Generator) is an Open Source Software, which automatically generates a wide variety of events for the testing phases of any event-processing program, using a user-friendly method, to obtain a solid and reliable CEP system. 

The project which contains IoT-TEG can be downloaded from https://gitlab.com/ucase/public/iot-teg

The project contains 3 main software components:

* ExprLang component (ioteg-exprlang)
* The IoT-TEG server application (iotteg)
* The IoT-TEG client application (ioteg-frontend)

## Requirements 

The requirements of the computer:

* Internet connection
* RAM 4Gb
* At least 2Ghz processor

Moreover, it is necessary to install: 

* MariaDB - An user with all privilege must be created. This user creates an empty BD (which IoT-TEG will use) (see [MariaDB instalation guide](MariaDBInstalationGuide.md))
* Mosquitto - Unncoment or type: port 1883, listener 9001, protocol websockets (see [Mosquito instalation guide](MosquittoInstalationGuide.md))
* Java 8
* Maven and NPM
* Serve - It is a javascript package, it can be installed using Node.js NPM; npm install -g serve

## Installation

The installation of IoT-TEG must be done using the console. From this point of the installation guide, we supose that the project has been donwloaded as well as the required software have been installed.

We are located in ./ioteg folder. Our first step is to compile and install the ExprLang source code:

```
cd ioteg-exprlang/
mvn install
```

Once that ExprLang has been installed, we have to move in to the IoT-TEG Backend folder:

```
cd ioteg/src/main/resources
```

We need to create a file with the BD configuration and the Mosquitto server conection. Its content is the following:

```
spring.banner.location=banner.txt
spring.jpa.hibernate.ddl-auto=update

## It is neccesary to create the BD
# create database <bd_name>

spring.datasource.url=jdbc:mariadb://<bd_url>:<bd_port>/<bd_name>
spring.datasource.username=<BDuser>
spring.datasource.password=<BDpassword>
spring.jpa.open-in-view=false
spring.jpa.properties.javax.persistence.validation.mode=none

mqtt.user=<mqtt user>
mqtt.password=<mqtt password>
mqtt.server=tcp://<mqtt_url>:<mqtt_port>
```

The lines spring.datasource.* are the ones to modify according to the requested data. Moreover, the mqtt.* lines have to be modified in order to allow IoT-TEG connects to MQTT Broker.

Once this file has been created and saved, we must package the application. Move to the ioteg folder where the pom.xml is stored:

<pre>
 cd ../../../
 mvn package -DskipTests
</pre>

The .jar created is saved in ./ioteg/target folder.

Now we have to configurate the source code of the client. We change to the ioteg-frontend folder and modify the .env file. We musth indicate the Mosquitto URL broker, user, password, as well as the API REST URL (where the server aplication is stored):

```
REACT_APP_MQTT_BROKER=<mqtt_url>
REACT_APP_MQTT_USERNAME=<mqtt user>
REACT_APP_MQTT_PASSWORD=<mqtt password>
REACT_APP_API_BASE_URL=<api_endpoint_url>
```

Once the .env file has been modified, it is time to compile the client code. Staying in the same location type:

```
npm run build
```

After the compilation, in build folder, the client application has been created.

## To run IoT-TEG

To run the server we have to move to the ./ioteg/target folder and type the following command, which will use the obtained .jar:

```
java -jar -Dspring.profiles.active=production ioteg.jar &
```

Once the server application is running, the API REST will be running on the 8080 port.

To run the client application, we have to move to the build folder ./ioteg-frontend/build and type:


```
serve &
```

Now, we can access using a web browser to the 5000 port and the IoT-TEG welcome page will be displayed.

## Contact

Report errors or ask for further information to Lorena Gutiérrez-Madroñal lorena.gutierrez@uca.es
_
IoT-TEG has been legally registered in the "Registro Territorial de la Propiedad Intelectual de la Junta de Andalucía" with the code: CA-00038-2017_

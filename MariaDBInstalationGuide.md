## Install MariaDB

Update the repositories of your computer

```
~$ sudo apt update
```

To update your system:

```
~$ sudo apt upgrade
```

Once that your system is updated we install the MariaDB software:

```
~$ sudo apt install mariadb-server
```

## Configure MariaDB

```
~$ sudo mysql_secure_installation
```

The following questions should be answered as we indicate:

- Enter current passpord for root (enter for none): enter
- Set root password?: n
- Remove anonymous users?: Y
- Disallow root login remotely?: Y
- Remove test database and access to it?: Y
- Reload privilege tables now?: Y

## Execute MariaDB and create a user

You can access to MariaDB typing:

```
~$ sudo mysql
```

Now that we are running MariaDB, we have to create a user granting all the privileges:

```
MariaDB[(none)]> GRANT ALL ON *.* TO 'user_name'@'localhost' IDENTIFIED BY 'user_password' WITH GRANT OPTION;
MariaDB[(none)]> FLUSH PRIVILEGES;
```

Exit from MariaDB to check that it has been installed successfully:

```
MariaDB[(none)]> exit;
```

and then:

```
~$ sudo systemctl status mariadb
```

It should appears that it is Active (running).

Enter to MariaDB with the created user:

```
~$ mysql -u user_name -p
```

You have to enter the password.

Then create the DB which will used by IoT-TEG.

```
MariaDB[(none)]> CREATE DATABASE IoTTEGdb;
```

To be sure, that our user is granted with all the privileges we can type:

```
MariaDB[(none)]> GRANT ALL PRIVILEGES ON `IoTTEGdb`.* TO 'user_name'@'localhost';
```

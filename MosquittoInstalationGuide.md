## To install Mosquitto and MQTT broker

```
~$ apt-get install mosquitto mosquitto-clients
```

## To configure Mosquitto

Create an user (the user in the example is called mosquitto):

```
~$ adduser mosquitto 
```

Copy the configuration template to our service:

```
~$ cp /etc/mosquitto/mosquitto.con.example /etc/mosquitto/mosquitto.conf
```

And you should modify the following information:

```
pid_file /var/run/mosquitto.pid

log_dest file /var/log/mosquitto/mosquitto.log
include_dir /etc/mosquitto/conf.d

listener 9001
port 1883
protocol websockets
persistence true

persistence_location /var/lib/mosquitto/
persistence_file mosquitto.db

log_dest syslog
log_dest stdout
log_dest topic

log_type error
log_type warning
log_type notice
log_type information

connection_messages true
log_timestamp true
allow_anonymous false
password_file /etc/mosquitto/pwfile
```

Save the file and check if the configuration is right:

```
~$ mosquitto -c /etc/mosquitto/mosquitto.conf
```

Reboot the service:

```
~$ service mosquitto restart
```

## Create an user

Given that we have specified that we not allow anonymous users, we have to create a new one:

```
~$ mosquitto_passwd -c /etc/mosquitto/pwfile [name]
```

Now the broker is ready.
